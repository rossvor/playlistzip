from playlistzip import __version__
from playlistzip import cli

def test_version():
    assert __version__ == '0.1.0'

def test_combine_episodes():
    episode_dicts = [
        {
            3: ['Third title', '0ea2c9'],
            4: ['Fourth title', 'a79923'],
            5: ['Fifth title', '646082'],
            6: ['Sixth title', 'c8637d'],
        },
        {
            11: ['B title', '69a25f'],
            5:  ['B title', 'b312ea'],
            4:  ['B title', 'e9cd50'],
            23: ['B title', '9c41cf'],
        },
        {
            5:  ['C title', 'e8e13d'],
        },
    ]

    expected = {
        4: ( 'Fourth title', ['a79923', 'e9cd50', None] ),
        5: ( 'Fifth title', ['646082', 'b312ea', 'e8e13d'] ),
    }

    assert cli.combine_episodes(episode_dicts) == expected
